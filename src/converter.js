/**
 * Padding always outputs 2 characters
 * @param {string} hex
 * @returns {string}
 */

 const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
 }

 module.exports = {
     /**
      * Converts RGB to Hex string
      * @param {number} red
      * @param {number} green
      * @param {number} blue
      * @returns {string}
      */

      rgbToHex: (red,green,blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        return pad(redHex) + pad(greenHex) + pad(blueHex);
      },

      hexToRgb: (hex) => {
        const redRGB = parseInt(hex.substring(0, 2), 16);
        const greenRGB = parseInt(hex.substring(2, 4), 16);
        const blueRGB = parseInt(hex.substring(4, 6), 16);
        return [redRGB, greenRGB, blueRGB];
      }
 }