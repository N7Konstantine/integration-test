const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () =>{
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0);
            const greenHex = converter.rgbToHex(0, 255, 0);
            const blueHex = converter.rgbToHex(0, 0, 255);

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });

    describe("Hex to RGB conversion", function() {
        it("converts the basic colors", function() {
            const redRGB   = converter.hexToRgb("ff0000");
            const greenRGB = converter.hexToRgb("00ff00");
            const blueRGB  = converter.hexToRgb("0000ff");

            expect(redRGB).to.deep.equal([255, 0, 0]);
            expect(greenRGB).to.deep.equal([0, 255, 0]);
            expect(blueRGB).to.deep.equal([0, 0, 255]);
        });
    });
});